/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

/*
    print usage and die
*/    

void usage ()
{
    fprintf (stderr, 
"\n"
"ICCE Interactive Filechanger  V%s\n"
"Copyright (c) Karel Kubat / ICCE %s. All rights reserved.\n"
"Another MegaHard Production.\n"	     
"\n"
"fch by Karel Kubat (karel@icce.rug.nl).\n"
"\n"
"Usage: fch [-rav] [--] searchstring replacestring file(s)\n"
"where:\n"
"       -r:              (optional) backup files will be removed\n"
"       -a:              (optional) all lines in input file are shown\n"
"                        default: only lines with searchstring\n"
"       -v:              increases verbosity\n"
"       --               stop processing options, incase your strings or\n"
"                        files start with a hyphen\n"	     
"       searchstring:    the string to search for\n"
"       replacestring:   the string to replace searchstring with\n"
"       file(s):         files to process\n"
"The searchstring must exactly match the contents of the file(s) in order\n"
"to be found. When files are changed, .bak backup versions are kept (unless\n"
"the -r flag is given).\n"
"\n"
        , VER, YRS);
	
    exit (1);
}	

