/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

/*
    scan a whole file, change if required
*/

int scan (FILE *inf, FILE *outf)
{
    char
        buf [MAXBUF];
    register int
        ret = 0;
    register char
        *cp;

    /* should we stop processing? */
    if (ans == ans_quit)
        return (0);

    /* reset answer if last response was Y or N */
    if (ans == ans_changethisfile || ans == ans_leavethisfile)
        ans = ans_none;

    /* loop thru file */
    while (1)
    {
        if (! fgets (buf, MAXBUF - 1, inf))
	    break;
	    
	cp = buf;
	while ( (cp = strstr (cp, searchstring)) )
	    if (query (&ans, buf, cp))
	    {
	        ret = 1;
	        changebuf (buf, cp);
		cp += strlen (replacestring);
	    }
	    else
	        cp += strlen (searchstring);
	
	if (! silent)
	    printf ("%s", buf);
	fprintf (outf, "%s", buf);
    }
    
    return (ret);
}
