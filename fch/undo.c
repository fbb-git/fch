/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

/*
    when a file wasn't changed: undo the preparations
    i.e. remove file, rename the backup file to original name
*/

void undo (char *backname, char *filename)
{
    if (unlink (filename))
        error ("can't remove redundant new file %s\n"
	       "\tthe original contents are in the backup %s", 
            filename, backname);
	   
    if (rename (backname, filename))
        error ("can't rename backup %s to original name %s",
	    backname, filename);
}
