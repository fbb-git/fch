/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

/*
    change char buffer buf's occurrence of searchstring to replacestring
    at buf position where; where is assumed to point somewhere in buf
*/    

void changebuf (char *buf, char *where)
{
    char
        newbuf [MAXBUF];                        /* tmp buffer */
    register char
        *newwhere;                              /* where ptr in newbuf */
	
    strcpy (newbuf, buf);
    newwhere = newbuf + (int) (where - buf);
    strcpy (newwhere, replacestring);
    strcpy (newwhere + strlen (replacestring),
        where + strlen (searchstring));
	
    strcpy (buf, newbuf);
}
