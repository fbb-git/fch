/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

/*
    show message when verbose is on
*/    

void message (char *fmt, ...)
{
    va_list
        args;
	
    if (verbose)
    {
        va_start (args, fmt);
        fputs ("fch: ", stderr);
        vfprintf (stderr, fmt, args);
        fputc ('\n', stderr);
    }
}
