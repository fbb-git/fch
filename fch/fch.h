/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#ifndef NOMALLOC			/* for systems that lack malloc.h */
#include <malloc.h>
#endif

#ifdef NOOPTIND				/* forced declaration */
extern int optind;
extern int getopt (int ac, char **av, char *opt);
#endif

#ifndef VER				/* version, if undefined */
#define VER "-.--"
#endif

#ifndef YRS				/* same for the years */
#define YRS "----"
#endif

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#ifndef SIMPLEKEYS
#   include <termios.h>
#endif
#include <unistd.h>
#include <sys/stat.h> 

#define MAXBUF                  (1024*20)

typedef enum
{
    ans_none,
    ans_changethis,
    ans_leavethis,
    ans_changethisfile,
    ans_leavethisfile,
    ans_changeall,
    ans_quit,
} ANSWER_;

extern ANSWER_
    ans;

extern int
    exit_status,
    remove_backup,
    verbose,
    silent;

extern char
    *replacestring,
    *searchstring,
    version [],
    years [];

extern char
    *makebackup (char *filename);
    
extern int
    getinput (void),
    query (ANSWER_ *ap, char *buf, char *where),
    scan (FILE *inf, FILE *outf);
    
extern void 
    error (char *fmt, ...),
    change (char *filename),
    changebuf (char *buf, char *where),
    message (char *fmt, ...),
    promote (char *backname, char *filename),
    undo (char *backname, char *filename),
    usage (void),
    warning (char *fmt, ...);

    
