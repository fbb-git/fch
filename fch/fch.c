/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

/*
    main function of program:
    parses flags, sets global vars search/replacestring,
    calls change() to process all files
*/

int main (int argc, char **argv)
{
    register int
	opt,
        i;
    struct stat
	statbuf;

    while ( (opt = getopt (argc, argv, "ravh?-")) != -1 )
	switch (opt)
	{
	    case 'r':
		remove_backup = 1;
		break;
	    case 'a':
		silent = 0;
		break;
            case 'v':
                verbose++;
                break;
	    case '-':
		break;
	    default:
		usage ();
	}
        
    if (argc - optind < 3)
        usage ();
	
    message (remove_backup ? 
        "backup files will be removed" : 
	"backup files will be kept");
    message (silent ? 
        "only lines matching searchstring will be shown" :
        "all lines in input will be shown");
	
    searchstring  = argv [optind];
    replacestring = argv [optind + 1];
    
    message ("searchstring: %s", searchstring);
    message ("replacement : %s", replacestring);
    
    for (i = optind + 2; i < argc; i++)
    {
        message ("processing file: %s", argv [i]);

	if (stat (argv [i], &statbuf))
	    warning ("can't stat %s, skipped (%s)\n", 
	             argv [i], strerror (errno));
	else if (! S_ISREG (statbuf.st_mode))
	    warning ("%s is not a regular file, skipped\n", argv [i]);
	else
	{
	    change (argv [i]);
	    if (chmod (argv [i], statbuf.st_mode))
		warning ("problem resetting mode of %s (%s)\n", 
		         argv [i], strerror (errno));
	}
    }
	
    return (exit_status);
}
