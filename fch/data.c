/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

ANSWER_
    ans = ans_none;                     /* user's answer */

int
    exit_status = 0,                    /* exit status of prog, != 0 on
                                           errors or warnings */

    silent = 1,				/* 0 if -a flag given, note:
					   by default fch is silent */
                                           
    verbose = 0,                        /* verbosity level, 0 is off */
					   
    remove_backup = 0;                  /* 1 if -r flag given */

char
    *replacestring,                     /* search/replace strings */
    *searchstring;
    
