/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

/*
    makes backup name of file filename, renames file to backup name
    returns backupname

    backup names are file.bak
*/    

char *makebackup (char *filename)
{
    register char
        *ret;
	
    ret = malloc (strlen (filename) + 1 + 4);
    sprintf (ret, "%s.bak", filename);
    
    if (rename (filename, ret))
        error ("can't rename %s to backup file %s", filename, ret);
	
    return (ret);
}
