/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

/*
    change all searchstrings into replacestrings
    in file filename
*/

void change (char *filename)
{
    register char
        *backupname;                        /* name of backup file */
    register int
        changed;                            /* file changed or not? */
    register FILE
        *inf,                               /* in/out files */
	*outf;
	
    printf ("fch: file %s\n", filename);
    backupname = makebackup (filename);     /* make backup name */

    /* open backupfile for as input file, original file for output */
    if (! (inf  = fopen (backupname, "r")) )
        error ("can't open %s for reading", backupname);
    if (! (outf = fopen (filename, "w")) )
        error ("can't open %s for writing\n"
	       "\told file contents are in %s", filename, backupname);
	       
    changed = scan (inf, outf);                /* do the changes */
    
    fclose (inf);                              /* close files */
    fclose (outf);
    
    if (! changed)
        undo (backupname, filename);           /* rename .bak to org if */
    else                                       /* not changed */
        promote (backupname, filename);
	
    free (backupname);
}
