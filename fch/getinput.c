/* <<< Start of information >>>                                        */
/* This sourcefile is part of the "fch" program, the interactive file  */
/* changer.                                                            */
/* Copyright (c) ICCE / K. Kubat (karel@icce.rug.nl).                  */
/* Don't modify any file that's part of this package without consent   */
/* of the author. If you have suggestions, bug reports -- or better    */
/* yet, ready-to-insert improvements, mail them to me and I'll include */
/* them. You are however allowed and encouraged to distribute this     */
/* package,provided that you leave all files (and this notice) intact. */
/* <<< End of information >>>                                          */

#include "fch.h"

int getinput ()
{
    int
	ch = 0;
#ifdef SIMPLEKEYS
    /* without termios support */
    char
    	buf [80];
	
    gets (buf);
    ch = buf [0];
    
#else
    /* for termios support */
    struct termios
	tattr,
	saved;

    tcgetattr(fileno (stdin), &saved);
    tcgetattr(fileno (stdin), &tattr);   
    tattr.c_lflag &= ~(ICANON | ECHO);
    tattr.c_cc[VMIN] = 1;
    tattr.c_cc[VTIME] = 0;
    tcsetattr(STDIN_FILENO, TCSAFLUSH, &tattr);

    fflush (stdout);
    fflush (stderr);
    
    read (fileno (stdin), &ch, 1);
    printf ("%c\n", ch);

    fflush (stdout);

    tcsetattr(fileno (stdin), TCSANOW, &saved);
#endif	/* SIMPLEKEYS */

    return (ch);
}
